import React from 'react'

import Item from './item.js'

class Basket extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      items : [
        {
          id: 1,
          name : "Mountain Dew",
          quantity :1,
          rate : 10
        },
        {
          id: 2,
          name : "Desparados",
          quantity :1,
          rate : 50
        },
        {
          id: 3,
          name : "Jack Daniels",
          quantity :0,
          rate : 100
        }
      ],
      total : 0
    }
    this.handleDelete = this.handleDelete.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleClear = this.handleClear.bind(this);
  }

  componentDidMount() {
    this.setState({total : this.reTotal()})
  }

  componentDidUpdate() {
    let newTotal = this.reTotal()
    if (this.state.total !== newTotal) this.setState({total : newTotal})
  }

  reTotal() {
    let total = 0;
    this.state.items.forEach(eachItem => {
      total += (eachItem.rate * eachItem.quantity)
    })
    return total
  }

  handleDelete(id) {
    const copyState = this.state.items.filter(item => item.id !== id)
    this.setState({ items: copyState});
  }

  handleChange(event){
    const newValue = event.target.value;
    const index = this.state.items.findIndex(item => item.id == event.target.name);

    this.setState(prevState => {
      prevState.items[index].quantity = newValue
      return {
        ...prevState
      }
    })
  }

  handleClear() {
    this.setState(prevState => {
      prevState.items.map(eachItem => eachItem.quantity=0)
      return {
        ...prevState
      }
    })
  }

  render() {
    return (
      <div id="basket">

        <div id="item_table">
          {
              this.state.items.map(eachItem => {
                return (
                  <Item key={eachItem.id} item={eachItem} onDelete={this.handleDelete} onChange={this.handleChange}/>
                )
            })
          }
        </div>

        <div id="basket_controls">
          <span id="total_amount"> ${this.state.total}</span>
          <button id="checkout_button" className="btn">Checkout</button>
          <button className="btn" onClick={this.handleClear}>Clear</button>
        </div>

      </div>
    )
  }
}

export default Basket
