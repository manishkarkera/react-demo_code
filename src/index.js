// import '@assets/favicon.ico'
import '@styles/styles.scss'
// // import trump from '@images/trump.jpg'


import React from 'react'
import ReactDOM from 'react-dom'
import Basket from './basket.js'
class App extends React.Component {
  render() {
    return (
      <div>
        <Basket />
      </div>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('root'))
