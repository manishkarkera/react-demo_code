import React from 'react'

class Item extends React.Component{
  render() {
    return(
      <div className="item_row">
        <span className="item_description">{this.props.item.name} </span>
        <span className="item_quantity"> <input name={this.props.item.id} onChange={this.props.onChange} type="number" value={this.props.item.quantity}/> </span>
        <span className="item_cost">$ {this.props.item.rate * this.props.item.quantity}</span>
        <span className="item_remove"><span onClick={() => this.props.onDelete(this.props.item.id)}>x</span></span>
      </div>
    )
  }
}

export default Item
